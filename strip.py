#! /usr/bin/env python3

import sys
import os

version_from = '0.12.0'

DIR = os.path.dirname(os.path.abspath(__file__))

with open(os.path.join(DIR, 'RELEASED'), 'r+') as h:
    existing_versions = [x.strip() for x in h.read().splitlines() if x]

with open(os.path.join(DIR, '.releaseignore'), 'r+') as h:
    ignored = [x.strip() for x in h.read().splitlines() if x]

all_versions = [x.strip() for x in sys.stdin.read().splitlines() if x]

# Output versions until we get to the first version the patch applies to
# Note: Versions that are already published are skipped as well
for version in all_versions:
    if version in ignored:
        # Skip this one
        continue

    if version not in existing_versions:
        print(version)

    if version == f"v{version_from}":
        break
