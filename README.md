# cert-manager with proxy protocol self-checks

This repository contains a toolkit to automatically build and release versions of [jetstack/cert-manager](https://github.com/jetstack/cert-manager) with a patch that adds [proxy protocol](https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt) headers to self-check requests. This is a workaround for https://github.com/jetstack/cert-manager/issues/466 which occurs in some managed kubernetes platforms (like Digitalocean when proxy protocol is enabled in LB).

The images themselves are available on [docker hub](https://hub.docker.com/u/jyrno42).

## How does it work?

The repository contains a minimal [patch](./patches/default.diff) which wraps net.Conn and prepends a dummy proxy protocol header when writing a chunk of data to the socket. This is enough to make the issue dissapear as the ingress controller receives the request with PROXY header and won't error with `2018/04/13 12:27:55 [error] 1837#1837: *10321 broken header: "GET /.well-known/acme-challenge/` (truncated) anymore. 

Next piece of the puzzle is a make commmand `build-tags` which clones the original repository and loads all of its tags. Once the tags are loaded they are passed to [strip.py](./strip.py) which outputs the versions until it encounters 0.12.0 (which is the first version which has multiarch support). 

Those versions are then passed onto [bob](./bob) which checks out the correct version (git tag) and tries to apply the patch. If patch applies cleanly we then build plus tag the docker images and push them to docker hub. The version number is then appended to [RELEASED](./RELEASED) file so we can avoid building rebuilding unless explicitly needed. If the patch does not apply cleanly then the script errors and manual work is needed to create a version specific patch (for example if `patches/v0.14.0.diff` exists then it will be used when building v0.14.0).

All of this will be executed via Gitlab CI every morning via [pipeline schedules](https://gitlab.com/help/user/project/pipelines/schedules) to automatically generate new releases when they are available in upstream. The CI builds will also generate a commit and an MR if the run results in changes to the RELEASED file.
