#!/bin/bash

set -e

while read version
do
    echo -e "\033[0;36mBuilding version ${version}\033[0m"

    # Checkout correct version and apply the patch
    git checkout ${version} > /dev/null 2>&1

    if [[ -f ../../patches/${version}.diff ]]; then
        echo "Using version ${version} patch"
        git apply ../../patches/${version}.diff
    else
        echo "Using default patch"
        git apply ../../patches/default.diff
    fi

    chmod +x do-build.sh

    # Update image version in buildscript
    sed -i "s/TPL_VERSION/${version}/g" do-build.sh

    # Build and push to the repository
    ./do-build.sh

    # Remember that we have built that version
    echo ${version} >> ../../RELEASED

    # Clean up
    (cd ../../ && make reset) > /dev/null 2>&1

    echo ""
    echo ""
    echo ""

done < "${1:-/dev/stdin}"
